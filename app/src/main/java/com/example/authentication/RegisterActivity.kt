package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.content.Intent
import android.widget.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class RegisterActivity : AppCompatActivity() {

    private lateinit var  btn_register: Button
    private lateinit var et_register_email: EditText
    private lateinit var et_register_password: EditText
    private  lateinit var tv_login1: TextView
    private lateinit var fl_header_image1: FrameLayout
    private lateinit var tv_title1: TextView
    private lateinit var til_register_email: TextInputLayout
    private lateinit var til_register_password: TextInputLayout
    private lateinit var tv_don_t_have_an_account1:TextView
    private lateinit var til_register_name:TextInputLayout
    private lateinit var et_register_name:TextView


    private lateinit var database: FirebaseDatabase
    private lateinit var referance: DatabaseReference


    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("UserInfo")



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


        btn_register = findViewById(R.id.btn_register)
        et_register_email = findViewById(R.id.et_register_email)
        et_register_password = findViewById(R.id.et_register_password)
        tv_login1=findViewById(R.id.tv_login1)
        fl_header_image1=findViewById(R.id.fl_header_image1)
        tv_title1=findViewById(R.id.tv_title1)
        til_register_email=findViewById(R.id.til_register_email)
        til_register_password=findViewById(R.id.til_register_password)
        tv_don_t_have_an_account1=findViewById(R.id.tv_don_t_have_an_account1)
        til_register_name=findViewById(R.id.til_register_name)
        et_register_name=findViewById(R.id.et_register_name)


        database = FirebaseDatabase.getInstance();
        referance = database.getReference("User")














        tv_login1.setOnClickListener {
            onBackPressed()
        }





        btn_register.setOnClickListener {
            when {
                TextUtils.isEmpty(et_register_email.text.toString().trim { it <= ' ' }) ->{
                    Toast.makeText(
                        this@RegisterActivity,
                        "Please enter email.",
                        Toast.LENGTH_SHORT
                    ).show()
                }


                TextUtils.isEmpty(et_register_password.text.toString().trim { it <= ' ' }) -> {
                    Toast.makeText(
                        this@RegisterActivity,
                        "Please enter password.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {

                    val email: String = et_register_email.text.toString().trim { it <= ' ' }
                    val password: String = et_register_password.text.toString().trim { it <= ' ' }


                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener { task ->

                            if (task.isSuccessful) {

                                val firebaseUser: FirebaseUser = task.result!!.user!!

                                Toast.makeText(
                                    this@RegisterActivity,
                                    "You were registered successfully.",
                                    Toast.LENGTH_SHORT
                                ).show()


                                val intent =
                                    Intent(this@RegisterActivity, MainActivity::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                intent.putExtra("user_id", firebaseUser.uid)
                                intent.putExtra("email_id", email)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(
                                    this@RegisterActivity,
                                    task.exception!!.message.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                        }
                }

            }
        }

        btn_register.setOnClickListener{

            val email: String = et_register_email.text.toString()
            val name: String = et_register_name.text.toString()

            val user = User(name,email)

            db.child(auth.currentUser?.uid!!).setValue(user)

        }





        btn_register.setOnClickListener{
            val intent = Intent(this,MainActivity::class.java)

            startActivity(intent)
        }
    }







}