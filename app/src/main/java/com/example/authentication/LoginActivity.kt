package com.example.authentication

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var tv_register: TextView
    private lateinit var btn_login: Button
    private lateinit var et_login_email: EditText
    private lateinit var et_login_password: EditText
    private lateinit var fl_header_image: FrameLayout
    private lateinit var tv_title: TextView
    private lateinit var til_login_email: TextInputLayout
    private lateinit var til_login_password: TextInputLayout
    private lateinit var tv_don_t_have_an_account: TextView
    private lateinit var forgot_your_password: TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        tv_register = findViewById(R.id.tv_register)
        btn_login=findViewById(R.id.btn_login)
        et_login_email=findViewById(R.id.et_login_email)
        et_login_password=findViewById(R.id.et_login_password)
        fl_header_image=findViewById(R.id.fl_header_image)
        tv_title=findViewById(R.id.tv_title)
        til_login_email=findViewById(R.id.til_login_email)
        til_login_password=findViewById(R.id.til_login_password)
        tv_don_t_have_an_account=findViewById(R.id.tv_don_t_have_an_account)
        forgot_your_password=findViewById(R.id.forgot_your_password)


        forgot_your_password.setOnClickListener{
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Forgot Password")
            val view = layoutInflater.inflate(R.layout.dialog_forgot_password,null)
            val username = view.findViewById<EditText>(R.id.et_username)
            builder.setView(view)
            builder.setPositiveButton("Reset", DialogInterface.OnClickListener {  _, _  ->
                forgotPassword(username)

            })
            builder.setNegativeButton("Close", DialogInterface.OnClickListener {  _, _  ->  })
            builder.show()
        }










        tv_register.setOnClickListener {
            startActivity(Intent(this@LoginActivity,RegisterActivity::class.java))

        }

        btn_login.setOnClickListener {

            when{
                TextUtils.isEmpty(et_login_email.text.toString().trim { it <= ' ' }) -> {
                    Toast.makeText(
                        this@LoginActivity,
                        "Please enter email.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                TextUtils.isEmpty(et_login_password.text.toString().trim { it <= ' ' }) -> {
                    Toast.makeText(
                        this@LoginActivity,
                        "Please enter password.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val email: String = et_login_email.text.toString().trim { it <= ' ' }
                    val password: String = et_login_password.text.toString().trim { it <= ' ' }

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener { task ->

                            if (task.isSuccessful){

                                Toast.makeText(
                                    this@LoginActivity,
                                    "You are logged in successfully.",
                                    Toast.LENGTH_SHORT
                                ).show()

                                val intent =
                                    Intent(this@LoginActivity,MainActivity::class.java)
                                intent.flags=
                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                intent.putExtra(
                                    "user_id",
                                    FirebaseAuth.getInstance().currentUser!!.uid
                                )

                                intent.putExtra("email_id",email)
                                startActivity(intent)
                                finish()
                            } else {

                                Toast.makeText(
                                    this@LoginActivity,
                                    task.exception!!.message.toString(),
                                    Toast.LENGTH_SHORT

                                ).show()
                            }
                        }
                }
            }
        }

        tv_register.setOnClickListener{
            val intent = Intent(this,RegisterActivity::class.java)

            startActivity(intent)
        }

        btn_login.setOnClickListener{
            val intent = Intent(this,MainActivity::class.java)

            startActivity(intent)
        }
    }






    private fun forgotPassword(username: EditText){

        if (username.text.toString().isEmpty()) {
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(username.text.toString()).matches()) {
            return
        }

        var mAuth: FirebaseAuth? = null

        mAuth = FirebaseAuth.getInstance()
        mAuth!!.sendPasswordResetEmail(username.text.toString())
            .addOnCompleteListener { task -> if (task.isSuccessful) {
                Toast.makeText(this,"Email sent.",Toast.LENGTH_SHORT).show()
            }

                else {
                Toast.makeText(this,"Email has not been sent.",Toast.LENGTH_SHORT).show()
                }
                }


    }




}